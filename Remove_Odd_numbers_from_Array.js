function evensOnly(arr) { // this one may be implemented with reduce as well, if you need example to practice on
  var newArr = []
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] % 2 === 0) {
      newArr.push(arr[i]);
    }

  }
  return newArr;
}

console.log(evensOnly([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]));
console.log(evensOnly([21, 26, 27, 28, 29]));

function evensOnly2(arr) { // 👍
  return arr.filter(function(e) {
    if (e % 2 === 0) {
      return e;
    }
  })
}

console.log(evensOnly2([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]));
console.log(evensOnly2([21, 26, 27, 28, 29]));
