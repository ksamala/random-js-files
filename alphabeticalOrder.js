function alphabeticalOrder(str) {
  return str.toLowerCase().split('').sort().join('').trim();
}

console.log(alphabeticalOrder('Kiran kumar'));

function alphaOrder(str) {
  str = str.toLowerCase(); // in general it's better to avoid reassignment of passed aguments (if you'd like to debug you don't have access to initial parameter value)
  var strArr = [...str]; // 👍, may be achieved with str.split('')
  return strArr.sort().join('').trim() // you can make it single liner -> str.toLowerCase().split('').sort().join('').trim()
}

console.log(alphaOrder('Kiran Kumar Samala'))
