function repeatStringFor(str, num) {
  var repStr = '';
 for (var i = 1; i <= num; i++) {
   repStr += str;
 }
  return repStr;
}

console.log(repeatStringFor('car', 5));
console.log(repeatStringFor('nill', 10));
console.log(repeatStringFor('car', -5));

function repeatStringWhile(str, num) {
  var repStr = '';
  var i = 1;
  while(i <= num) {
    repStr += str;
    i++;
  }
  return repStr;
}

console.log(repeatStringWhile('car', 5));
console.log(repeatStringWhile('nill', 10));
console.log(repeatStringWhile('car', -5));